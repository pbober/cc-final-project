-- Piotr Bober
-- Compiler Construction 2009/10
-- the data types

module DataTypes where

-- variable
type Variable = String

-- arithmetic expressions
data AExp = Const Double
          | Var Variable
          | FCall String Args
          | UnOp String AExp
          | BinOp String AExp AExp
--          | Conditional BExp AExp AExp
   deriving Show

-- function call arguments
type Args = [AExp]

-- boolean expressions
data BExp = FALSE
          | TRUE
          | Neg BExp
          | BoolBinOp String BExp BExp
          | Rel String AExp AExp
   deriving Show

-- statements
data Statement = If BExp CodeBlock CodeBlock
               | If' BExp CodeBlock
               | While BExp CodeBlock
               | Do BExp CodeBlock
               | Seq CodeBlock
               | PrintStr String
               | Print AExp
               | Read Variable
               | Return AExp
               | Assign Variable AExp
               | Init Variable AExp
               | Skip
   deriving Show

-- code block
type CodeBlock = [Statement]

-- function definition
data FunDef = FunDef String [Variable] CodeBlock
   deriving Show

-- program
type Program = [FunDef]

